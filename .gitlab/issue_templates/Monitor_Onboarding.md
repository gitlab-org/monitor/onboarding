# Title: Monitor:Respond Group Onboarding Issue for [name], starting [date]

Welcome [name] to the Monitor:Respond group! We're very excited to have you. This onboarding issue is meant to supplement your general PeopleOps onboarding issue with Monitor specific information and setup. There is nothing you need to do with this issue until your *second week*, so feel free to ignore it until then.

[onboarding buddy name] will be your team onboarding buddy. They will help you get setup and will work closely with you on your first issue. If you have any questions along the way, do not hesitate to reach out to [onboarding buddy name] or pose questions in the [#g_respond](hhttps://gitlab.slack.com/archives/C02SHPPGZS5) slack channel.

### First Day
* [ ] Manager: Add to "Monitor Stage" [google groups](https://groups.google.com/a/gitlab.com/g/monitor-stage/members) - this will also add them to the team calendar and drive.
* [ ] Manager: Add to both [Retrospective project](https://gitlab.com/gl-retrospectives/monitor/respond/-/project_members) and list of assignees on [retro issue template](https://gitlab.com/gitlab-org/async-retrospectives/-/edit/master/teams.yml)
* [ ] Manager: Invite new team member to the following slack channels: 
  * [ ] `#g_respond`
  * [ ] `#g_respond_be`
  * [ ] `#g_respond_fe`
  * [ ] `#g_respond_standup`
  * [ ] `#backend` or `#frontend`
  * [ ] `#development`
  * [ ] `#respond-donut`
* [ ] Manager: Ensure the new team member is added to the appropriate slack groups like: @monitor-respond-group, @monitor-be, @monitor-fe by opening an [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new)
* [ ] Manager: Add the new team member as a participant to your async standups:
  * [Monitor:Respond - Check-in](https://app.geekbot.com/dashboard/standup/46150/view)
  * [Monitor:Respond - Check-in+](https://app.geekbot.com/dashboard/standup/48846/view)
  * [Monitor:Respond - Random Wed](https://app.geekbot.com/dashboard/standup/84504/view)
* [ ] Manager: Add the new team member to the [Monitor Stage Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_1lc2dqin1h1t60qh6rfr2ce198%40group.calendar.google.com&ctz=America%2FLos_Angeles)

### First Week
* [ ] Onboarding buddy: Schedule a 30 minute call with the new team member to introduce yourself. Make sure to walk through the new team member's calendar and ensure that the team meeting and other meetings are visible.
* [ ] Onboarding buddy: Work with your engineering manager and product manager to identify the first issue that the new team member should tackle.
* [ ] New Team Member: Link PTO by Deel to Monitor Stage Calendar.
    * Go to PTO by Deel in slack -> Home -> Calendar Sync in dropdown -> Additional calendars to include?.  Add Monitor Stage calendar `gitlab.com_1lc2dqin1h1t60qh6rfr2ce198@group.calendar.google.com`.

### Second Week
* [ ] Onboarding buddy: Work closely with the new team member as they work on their development setup and on the issue we identified.
* [ ] New team member: Read information about the Respond Group:
  * [ ] [Respond Group page](https://about.gitlab.com/handbook/engineering/development/ops/monitor/respond/)
  * [ ] [Monitor direction page](https://about.gitlab.com/direction/monitor/)
  * [ ] [Incident Management direction page](https://about.gitlab.com/direction/monitor/debugging_and_health/incident_management)
  * [ ] [Why All Organizations Need Prometheus](https://about.gitlab.com/2018/09/27/why-all-organizations-need-prometheus/)
* [ ] New team member: Checkout the [Tanuki Inc project](https://gitlab.com/gitlab-examples/ops/incident-setup/everyone/tanuki-inc/). Talk to your Onboarding buddy if you are unable to access the projects.
* [ ] New team member: Watch the most recent Respond group kickoff call: https://youtube.com/playlist?list=PL05JrBw4t0KrUe3MhOsFQGSv2uoN_Ca_B
* [ ] New team member: Schedule 30 minute introductory call with our product manager
* [ ] New team member: Read about how Gitlab uses labels [Issue Workflow Labels](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/issue_workflow.md)
* [ ] New team member: Watch some Monitor-specific videos:
  * [ ] [Incident Management YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoO5WmppV5TduOD9D-IP2p0) (In YouTube go to `settings` --> `switch account` to change your profile to `GitLab Unfiltered` to view this playlist)
  * [ ] [Instrumenting applications for Prometheus](https://www.youtube.com/watch?v=e1-wIbQS-oE) (37m)

### Third Week
* [ ] New team member: Schedule coffee chats with each of your direct team members.
  * [ ] call with _____
  * [ ] call with _____
  * [ ] call with _____
  * [ ] call with _____

/confidential
