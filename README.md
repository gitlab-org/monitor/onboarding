# Onboarding - Monitor Group

This project helps the Monitor group onboard its new members.
An [issue template](.gitlab/issue_templates/Monitor_Onboarding.md) is used to create onboarding issues which supplement the [general onboarding](https://gitlab.com/gitlab-com/people-ops/employment/issues) issues. 

The first week for this onboarding issue has only tasks for the manager and the onboarding buddy. The second week has tasks for the new team member.
